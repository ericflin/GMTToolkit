package com.ericflin.gmttoolkit;

import android.content.Context;
import android.hardware.Camera;
import android.view.Display;
import android.view.Surface;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.WindowManager;

import java.util.List;

class Preview extends SurfaceView implements SurfaceHolder.Callback {

    SurfaceHolder mHolder;
    Camera mCamera;
    Context mContext;

    public Preview(Context context, Camera camera) {
        super(context);

        // Install a SurfaceHolder.Callback so we get notified when the
        // underlying surface is created and destroyed.
        mCamera = camera;
        mContext = context;

        mHolder = getHolder();
        mHolder.addCallback(this);
        mHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
        if (mHolder.getSurface() == null)
            return;

        try {
            mCamera.stopPreview();
        }
        catch (Exception e) {}

        try {
            Camera.Parameters parameters = mCamera.getParameters();
            List<Camera.Size> previewSizes = parameters.getSupportedPreviewSizes();
            height = previewSizes.get(0).height;
            width = previewSizes.get(0).width;

            Display display = ((WindowManager) mContext.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();

            // Some code adapted from http://stackoverflow.com/questions/3841122/android-camera-preview-is-sideways
            switch (display.getRotation()) {
                case Surface.ROTATION_0:
                //    parameters.setPreviewSize(height, width);
                    mCamera.setDisplayOrientation(90);
                    break;
                case Surface.ROTATION_90:
                //    parameters.setPreviewSize(width, height);
                    break;
                case Surface.ROTATION_180:
                //    parameters.setPreviewSize(height, width);
                    break;
                case Surface.ROTATION_270:
                //    parameters.setPreviewSize(width, height);
                    mCamera.setDisplayOrientation(180);
                    break;
            }

            mCamera.setParameters(parameters);

            mCamera.setPreviewDisplay(mHolder);
            mCamera.startPreview();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        if (mCamera != null)
            mCamera.stopPreview();
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        try {
            mCamera.setPreviewDisplay(holder);
            mCamera.startPreview();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onLayout(boolean changed, int l, int t, int r, int b) {}
}