package com.ericflin.gmttoolkit;

import android.content.Intent;
import android.location.Location;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.ericflin.gmttoolkit.R;

public class Move extends ActionBarActivity {
    Location mLocation;
    Double mBearing;

    // This is not strictly necessary (and in fact, it's quite unnecessary, because the declination
    // used is that from the location of the second bearing.)
    Double mDeclination;

    public static final String N_LOCATION = "first_location";
    public static final String N_BEARING = "first_bearing";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_move);

        Intent intent = getIntent();
        mLocation = intent.getParcelableExtra(MainActivity.M_LOCATION);
        mBearing = intent.getExtras().getDouble(MainActivity.M_BEARING);
        mDeclination = intent.getDoubleExtra(MainActivity.M_DECLINATION, 0);
    }

    public void secondBearing(View view) {
        Intent intent = new Intent(this, MainActivity.class);
        intent.putExtra(N_LOCATION, mLocation);
        intent.putExtra(N_BEARING, mBearing);
        intent.putExtra(MainActivity.M_DECLINATION, mDeclination);
        startActivity(intent);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_move, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
