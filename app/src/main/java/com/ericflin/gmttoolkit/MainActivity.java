package com.ericflin.gmttoolkit;

import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.hardware.Camera;
import android.hardware.GeomagneticField;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.provider.Settings;
import android.support.v4.view.MotionEventCompat;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.Surface;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Map;

/**
 * Handles grabbing the first and second bearings and locations, with {@link com.ericflin.gmttoolkit.Move}
 * as an intermediate to help guide the user. After the second bearing is gotten, this Class goes on to
 * DistanceCalculator.
 */
public class MainActivity extends ActionBarActivity {

    private Camera mCamera;
    private Preview mPreview;
    private SensorManager sensorManager;
    private LocationManager locationManager;
    private Sensor mSensorGravity;
    private Sensor mSensorMagneticField;
    private Sensor mSensorRotation;

    private double averagedSin;
    private double averagedCos;
    private Double averagedBearing;
    private double bearingStandardDeviation;
    private int bearingsCount;
    private boolean recordBearings = false;
    private Location bestLocation;

    GeomagneticField geomagneticField;
    private Location firstLocation;
    private Double firstBearing;

    public static final int ONE_MINUTE = 1000 * 60;
    public static final String M_LOCATION = "second_location";
    public static final String M_BEARING = "second_bearing";
    public static final String M_DECLINATION = "declination";

    public Camera getCamera() {
        stopPreviewAndFreeCamera();

        Camera c = null;
        try {
            c = Camera.open();
        }
        catch (Exception e) {
            e.printStackTrace();
        }

        return c;
    }

    private boolean hasCameraHardware(Context context) {
        return context.getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA);
    }

    public void stopPreviewAndFreeCamera() {
        if (mCamera == null)
            return;

        mCamera.stopPreview();
        mCamera.setPreviewCallback(null);
        mPreview.getHolder().removeCallback(mPreview);
        mCamera.release();
        mCamera = null;

        FrameLayout frame = (FrameLayout) this.findViewById(R.id.cameraPreview);
        frame.removeAllViews();
    }

    public boolean hasGeomagneticSensor(Context context) {
        return context.getPackageManager().hasSystemFeature(PackageManager.FEATURE_SENSOR_COMPASS);
    }

    // Checks to see if GPS is on and on high accuracy. Returns false if either not on or not high
    // accuracy mode.
    public boolean gpsIsOn() {
        ContentResolver contentResolver = getBaseContext().getContentResolver();
        try {
            int gpsStatus = Settings.Secure.getInt(contentResolver, Settings.Secure.LOCATION_MODE);

            if (gpsStatus == Settings.Secure.LOCATION_MODE_HIGH_ACCURACY) {
                return true;
            }
        }
        catch (Settings.SettingNotFoundException e) {
            e.printStackTrace();
        }

        return false;
    }

    public void showBearing() {
        DecimalFormat formatter = new DecimalFormat("#.00000");

        if (averagedBearing != null) {
            double declination = geomagneticField != null ? geomagneticField.getDeclination() : 0;
            Double bearing = averagedBearing + declination < 0 ? 360 + averagedBearing + declination : averagedBearing + declination;
            TextView bearingText = (TextView) this.findViewById(R.id.textView);
            bearingText.setText("Bearing: " + formatter.format(bearing) + "\u00B1" + new DecimalFormat("#.00").format(bearingStandardDeviation) + " (Decl: " + new DecimalFormat("#.0").format(declination) + ")");
        }

        showLocation();
    }

    public void showLocation() {
        DecimalFormat formatter = new DecimalFormat("#.00000");

        if (bestLocation != null) {
            TextView locationText = (TextView) this.findViewById(R.id.locationText);
            locationText.setText("Location: " + formatter.format(bestLocation.getLatitude()) + ", " + formatter.format(bestLocation.getLongitude()) + " (\u00B1" + new DecimalFormat("#.0ft").format(bestLocation.getAccuracy() * 3.280839895) + ")");

            Button nextButton = (Button) this.findViewById(R.id.main_next_button);
            nextButton.setEnabled(true);
        }
    }

    public void showMessage(String message) {
        ((TextView) this.findViewById(R.id.locationText)).setText(message);
        ((TextView) this.findViewById(R.id.textView)).setText(message);
    }

    public void clearLocationText() {
        TextView locationText = (TextView) this.findViewById(R.id.locationText);
        locationText.setText(R.string.get_a_bearing);
    }

    private View.OnTouchListener tapAndHoldGestureListener = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View view, MotionEvent e) {
            int action = MotionEventCompat.getActionMasked(e);

            switch (action) {
                // Start recording/averaging bearings. Actual recording of bearings happens in
                // the onSensorChanged method in mSensorEventListener.
                case MotionEvent.ACTION_DOWN:
                    averagedBearing = (double) 0;
                    bearingsCount = 0;
                    averagedSin = 0;
                    averagedCos = 0;
                    recordBearings = true;
                    // TODO: Don't clear the location when the screen is tapped. Instead, check to see
                    // if the user has moved a certain minimum distance and clear bestLocation as necessary.
                    bestLocation = null;
                    clearLocationText();
                break;

                // Stop recording/averaging bearings
                case MotionEvent.ACTION_UP:
                    recordBearings = false;
                break;
            }
            return true;
        }
    };

    public void startGPS() {
        if (!gpsIsOn())
            return;

        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        // Update location every 0.5 seconds and 0 ft. Set both to zero to waste battery and
        // potentially get higher accuracy. Shouldn't waste too much battery either way because
        // GPS updating is only on when user is holding down screen.
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 500, 0, mGPSListener);
    }

    private LocationListener mGPSListener = new LocationListener() {

        private void updateDeclination(Location location) {
            geomagneticField = new GeomagneticField(
                    Double.valueOf(location.getLatitude()).floatValue(),
                    Double.valueOf(location.getLongitude()).floatValue(),
                    Double.valueOf(location.getAltitude()).floatValue(),
                    System.currentTimeMillis()
            );
        }

        @Override
        public void onLocationChanged(Location location) {
            // If it's been a minute between this current location and the best location reading,
            // just replace the best location reading (phone has likely moved).
            if (bestLocation == null ||
                    Math.abs(bestLocation.getTime() - location.getTime()) > ONE_MINUTE ||
                    ! location.getProvider().equals(bestLocation.getProvider())) {
                bestLocation = location;
                updateDeclination(location);
                return;
            }

            // Compare accuracy between best location and this current location. Smaller numbers are better. 0 is not good.
            double accuracyDelta = location.getAccuracy() - bestLocation.getAccuracy();

            // Current location is more accurate than previous best location
            if (accuracyDelta < 0) {
                bestLocation = location;
                updateDeclination(location);
            }

            showLocation();
        }

        @Override
        public void onProviderDisabled(String provider) {}

        @Override
        public void onProviderEnabled(String provider) {}

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {}
    };

    public void startUpCameraAndPreview() {
        if (! hasCameraHardware(this))
            return;

        // Initialize camera
        mCamera = getCamera();
        mPreview = new Preview(this, mCamera);
        FrameLayout frame = (FrameLayout) this.findViewById(R.id.cameraPreview);
        frame.addView(mPreview);
        frame.setOnTouchListener(tapAndHoldGestureListener);
    }

    public void startUpCompass() {
        if (! hasGeomagneticSensor(this))
            return;

        // Initialize compass
        sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        mSensorGravity = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        mSensorMagneticField = sensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);

        if (mSensorGravity != null)
            sensorManager.registerListener(mSensorEventListener, mSensorGravity, SensorManager.SENSOR_DELAY_NORMAL);

        if (mSensorMagneticField != null)
            sensorManager.registerListener(mSensorEventListener, mSensorMagneticField, SensorManager.SENSOR_DELAY_NORMAL);
    }

    public void startUpRotationVectorSensor() {
        sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        mSensorRotation = sensorManager.getDefaultSensor(Sensor.TYPE_ROTATION_VECTOR);

        if (mSensorRotation != null)
            sensorManager.registerListener(mRotationSensorEventListener, mSensorRotation, SensorManager.SENSOR_DELAY_NORMAL);
    }

    public void nextBearing(View view) {
        if (bestLocation == null || averagedBearing == null) {
            showMessage("Please make sure you have a bearing and a location before continuing.");
            return;
        }

        Intent intent;
        if (firstLocation == null || firstBearing == null) {
            intent = new Intent(this, Move.class);
        }
        else {
            intent = new Intent(this, DistanceCalculator.class);
            intent.putExtra(Move.N_LOCATION, firstLocation);
            intent.putExtra(Move.N_BEARING, firstBearing);
        }

        intent.putExtra(M_LOCATION, bestLocation);
        intent.putExtra(M_BEARING, averagedBearing);

        if (geomagneticField != null)
            intent.putExtra(M_DECLINATION, Double.valueOf(geomagneticField.getDeclination()));

        startActivity(intent);
    }

    private SensorEventListener mRotationSensorEventListener = new SensorEventListener() {
        float[] mRotation;

        @Override
        public void onAccuracyChanged(Sensor sensor, int accuracy) {
        }

        @Override
        public void onSensorChanged(SensorEvent sensorEvent) {
            if (sensorEvent.sensor.getType() == Sensor.TYPE_ROTATION_VECTOR)
                mRotation = sensorEvent.values.clone();

            if (mRotation == null)
                return;

            float R[] = new float[9];

            SensorManager.getRotationMatrixFromVector(R, mRotation);
            float[] orientation = new float[3];
            SensorManager.getOrientation(R, orientation);

            if (recordBearings) {
                // Should never divide by zero unless bearingsCount is somehow -1.
                averagedSin = ((averagedSin * bearingsCount) + Math.sin(orientation[0])) / (bearingsCount + 1);
                averagedCos = ((averagedCos * bearingsCount) + Math.cos(orientation[0])) / (bearingsCount + 1);

                averagedBearing = Math.toDegrees(Math.atan2(averagedSin, averagedCos));
                bearingsCount++;

                showBearing();
            }
        }
    };

    private SensorEventListener mSensorEventListener = new SensorEventListener() {
        float[] mGravity;
        float[] mGeomagnetic;

        @Override
        public void onAccuracyChanged(Sensor sensor, int accuracy) {
            if (accuracy == SensorManager.SENSOR_STATUS_UNRELIABLE ||
                    accuracy == SensorManager.SENSOR_STATUS_ACCURACY_LOW ||
                    accuracy == SensorManager.SENSOR_STATUS_NO_CONTACT ) {
                recordBearings = false;
                showMessage("Please recalibrate your compass by rotating it in a figure-8 movement.");
            }
        }

        /*
         * time smoothing constant for low-pass filter
         * 0 ≤ alpha ≤ 1 ; a smaller value basically means more smoothing
         * See: http://en.wikipedia.org/wiki/Low-pass_filter#Discrete-time_realization
         */
        static final float ALPHA = 0.15f;

        /**
         * http://en.wikipedia.org/wiki/Low-pass_filter#Algorithmic_implementation
         * http://developer.android.com/reference/android/hardware/SensorEvent.html#values
         */
        protected float[] lowPass( float[] input, float[] output ) {
            if ( output == null ) return input;

            for ( int i=0; i<input.length; i++ ) {
                output[i] = output[i] + ALPHA * (input[i] - output[i]);
            }
            return output;
        }

        @Override
        public void onSensorChanged(SensorEvent sensorEvent) {
            if (!recordBearings)
                return;

            if (sensorEvent.sensor.getType() == Sensor.TYPE_ACCELEROMETER)
                mGravity = sensorEvent.values.clone(); // lowPass(sensorEvent.values.clone(), mGravity);
            if (sensorEvent.sensor.getType() == Sensor.TYPE_MAGNETIC_FIELD)
                mGeomagnetic = sensorEvent.values.clone(); // lowPass(sensorEvent.values.clone(), mGeomagnetic);

            if (mGravity == null || mGeomagnetic == null)
                return;

            float tmpR[] = new float[9];
            float I[] = new float[9];
            float R[] = new float[9];

            if(! SensorManager.getRotationMatrix(tmpR, I, mGravity, mGeomagnetic))
                return;

            double inclination = Math.acos(tmpR[8]);

            // Adjust coordinate system so azimuth matches direction camera is facing when phone is vertical.
            if (inclination >= Math.toRadians(25) && inclination <= Math.toRadians(155))
                SensorManager.remapCoordinateSystem(tmpR, SensorManager.AXIS_X, SensorManager.AXIS_Z, R);
            else
                R = tmpR.clone();

            float orientation[] = new float[3];
            SensorManager.getOrientation(R, orientation);

            // orientation[0]'s range is from -pi to +pi. Not sure how necessary it is to normalize
            // to 0 to 2*pi, but we'll do it anyway.
            double azimuth = orientation[0];
            if(azimuth < 0) {
                azimuth = (2 * Math.PI) + azimuth;
            }

            // Should never divide by zero unless bearingsCount is somehow -1.
            averagedSin = ((averagedSin * bearingsCount) + Math.sin(azimuth)) / (bearingsCount + 1);
            averagedCos = ((averagedCos * bearingsCount) + Math.cos(azimuth)) / (bearingsCount + 1);

            averagedBearing = Math.toDegrees(Math.atan2(averagedSin, averagedCos));

            // Using the Yamartino method for approximating the directional standard deviation of the
            // measurements of bearings:
            // double epsilon = Math.sqrt(1 - (Math.pow(averagedSin, 2) + Math.pow(averagedCos, 2)));
            // bearingStandardDeviation = Math.toDegrees(Math.asin(epsilon) * (1 + (((2 / Math.sqrt(3)) - 1) * Math.pow(epsilon, 3))));
            // Alternative method from: https://en.wikipedia.org/wiki/Directional_statistics#Distribution_of_the_mean and
            // http://stackoverflow.com/questions/13928404/calculating-standard-deviation-of-angles
            bearingStandardDeviation = Math.toDegrees(Math.sqrt(-Math.log(Math.pow(averagedSin, 2) + Math.pow(averagedCos, 2))));

            bearingsCount++;

            showBearing();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        Intent intent = getIntent();
        if (intent != null && intent.getExtras() != null) {
            firstLocation = intent.getParcelableExtra(Move.N_LOCATION);
            firstBearing = intent.getExtras().getDouble(Move.N_BEARING);
        }

        startUpCameraAndPreview();
        startUpCompass();
        startGPS();
        // startUpRotationVectorSensor();
    }

    @Override
    protected void onResume() {
        super.onResume();
        // sensorManager.registerListener(mRotationSensorEventListener, mSensorRotation, SensorManager.SENSOR_DELAY_NORMAL);
        sensorManager.registerListener(mSensorEventListener, mSensorGravity, SensorManager.SENSOR_DELAY_NORMAL);
        sensorManager.registerListener(mSensorEventListener, mSensorMagneticField, SensorManager.SENSOR_DELAY_NORMAL);

        startUpCameraAndPreview();
        startGPS();
    }

    @Override
    protected void onPause() {
        super.onPause();
        sensorManager.unregisterListener(mSensorEventListener, mSensorGravity);
        sensorManager.unregisterListener(mSensorEventListener, mSensorMagneticField);
        sensorManager.unregisterListener(mSensorEventListener);
        // sensorManager.unregisterListener(mRotationSensorEventListener, mSensorRotation);
        // sensorManager.unregisterListener(mRotationSensorEventListener);

        stopPreviewAndFreeCamera();
        locationManager.removeUpdates(mGPSListener);
/*

        FrameLayout frame = (FrameLayout) this.findViewById(R.id.cameraPreview);
        frame.removeView(mPreview);
*/
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
