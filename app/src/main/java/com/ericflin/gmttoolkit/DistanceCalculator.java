package com.ericflin.gmttoolkit;

import android.content.Intent;
import android.location.Location;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.TextView;

public class DistanceCalculator extends ActionBarActivity {

    private Button calcButton;
    private EditText[] bearings;
    private RadioGroup units;
    private TextView ans;

    private Double threeSixtyifyBearing(double bearing) {
        if (bearing < 0)
            return 360 + bearing;

        return bearing;
    }

    private void fillBoxesWithIntents() {
        Intent intent = getIntent();
        if (intent != null && intent.getExtras() != null) {
            Location firstLoc = intent.getParcelableExtra(Move.N_LOCATION);
            Location secondLoc = intent.getParcelableExtra(MainActivity.M_LOCATION);

            // Adjust for declination, because the secondLoc.bearingTo(firstLoc) method gives a bearing based on true north.
            Double declination = intent.getDoubleExtra(MainActivity.M_DECLINATION, 0);
            Double firstBearing = intent.getExtras().getDouble(Move.N_BEARING) + declination;
            Double secondBearing = intent.getExtras().getDouble(MainActivity.M_BEARING) + declination;

            ((EditText) findViewById(R.id.firstBearing)).setText(threeSixtyifyBearing(firstBearing).toString());
            ((EditText) findViewById(R.id.secondBearing)).setText(threeSixtyifyBearing(secondBearing).toString());

            ((EditText) findViewById(R.id.secondToFirst)).setText(threeSixtyifyBearing((double) secondLoc.bearingTo(firstLoc)).toString());

            // Convert meters to feet
            ((EditText) findViewById(R.id.dist)).setText(((Double) (secondLoc.distanceTo(firstLoc) * 3.280839895)).toString());
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_distance_calculator);

        fillBoxesWithIntents();

        calcButton = (Button) findViewById(R.id.button);
        bearings = new EditText[] { (EditText) findViewById(R.id.firstBearing),
                                    (EditText) findViewById(R.id.secondBearing),
                                    (EditText) findViewById(R.id.secondToFirst),
                                    (EditText) findViewById(R.id.dist) };
        units = (RadioGroup) findViewById(R.id.radioGroup);
        ans = (TextView) findViewById(R.id.answer);

        calcButton.setOnClickListener(new View.OnClickListener() {
            protected double[] arr_bearing = new double[4];

            private double toRad(int revolution, double x) {
                return 2 * Math.PI * (x/revolution);
            }

            private double milToRad(double x) {
                return toRad(3400, x);
            }

            private double degToRad(double x) {
                return toRad(360, x);
            }

            // Given bearings x, y, and z (in radians) and distance d, calculate distances e and f.
            private double[] calcDistRad(double x, double y, double z, double d) {
                // Bearing from first to second location is 180 degrees from bearing of second to first.
                double a1to2 = Math.toRadians(Math.toDegrees(Math.PI + z) % 360);
                // b is an obtuse angle when z - y > 180 degrees.
                double b = Math.abs(z - y) > Math.PI ? (2 * Math.PI) - Math.abs(z - y) : Math.abs(z - y);
                // a is an acute angle when b is an obtuse angle || it can't be over 180 degrees either way.
                double a = Math.abs(z - y) > Math.PI || Math.abs(a1to2 - x) <= Math.PI ? Math.abs(a1to2 - x) : (2 * Math.PI) - Math.abs(a1to2 - x);
                double c = Math.PI - a - b;
                if (c < 0) Log.d("", "Oops. x=" + (Math.toDegrees(x)) + ", y=" + (Math.toDegrees(y)) + ", z=" + (Math.toDegrees(z)));
                Log.d("", "a: " + ((Double) Math.toDegrees(a)).toString());
                Log.d("", "b: " + ((Double) Math.toDegrees(b)).toString());
                Log.d("", "c: " + ((Double) Math.toDegrees(c)).toString());

                return new double[] { ((Math.sin(b) * d)/Math.sin(c)),
                                      ((Math.sin(a) * d)/Math.sin(c)),
                                      c
                                    };
            }

            public void onClick(View v) {
                boolean err = false;

                for (int i = 0; i < bearings.length; i++) {
                    try {
                        arr_bearing[i] = Double.parseDouble(bearings[i].getText().toString());
                    } catch(Exception e) {
                        arr_bearing[i] = 0;
                        ans.setText("Please enter valid\nnumbers for all fields.");
                        err = true;
                    }

                    if (i != 3 && units.getCheckedRadioButtonId() == R.id.mils) {
                        arr_bearing[i] = milToRad(arr_bearing[i]);
                    }
                    else if (i != 3) {
                        arr_bearing[i] = degToRad(arr_bearing[i]);
                    }
                }

                if (!err) {
                    // TODO: Check to make sure negative values are not invalid and can simply be absolute value-d
                    double[] res = calcDistRad(arr_bearing[0], arr_bearing[1], arr_bearing[2], arr_bearing[3]);

                    if (res[2] >= 0)
                        ans.setText("Answer:\nDistance from first bearing:\n" + Long.toString(Math.abs(Math.round(res[0]))) +
                            " ft\nDistance from second:\n" + Long.toString(Math.abs(Math.round(res[1]))) + " ft");
                    else
                        ans.setText("\nYour measurements appear to be\npointing away from each other.\nAre you sure you were pointing\nat the same object?");
                }
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_distance_calculator, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void test() {

    }
}
