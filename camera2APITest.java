package com.ericflin.gmttoolkit;

import android.content.Context;
import android.content.Intent;
import android.graphics.ImageFormat;
import android.graphics.SurfaceTexture;
import android.hardware.camera2.*;
import android.hardware.camera2.params.StreamConfigurationMap;
import android.os.Handler;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Size;
import android.view.Menu;
import android.view.MenuItem;
import android.view.Surface;
import android.view.TextureView;
import android.view.View;
import android.widget.Button;

import java.util.Arrays;
import java.util.List;


public class MainActivity extends ActionBarActivity {

    private CameraDevice mCamera;
    private TextureView mTextureView;
    private Surface mPreview;
    private Size[] mSizes;
    private CaptureRequest.Builder mRequestBuilder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mTextureView = (TextureView) findViewById(R.id.textureView);
        mTextureView.setSurfaceTextureListener(surfaceTextureListener);

        setContentView(R.layout.activity_main);
    }

    /**
     * Given a CameraManager for a device, returns the String id of the rear-facing camera.
     *
     * @param manager
     * @return
     */
    private String getBackCameraId(CameraManager manager) {
        try {
            String[] cameraIds = manager.getCameraIdList();

            for (String cameraId : cameraIds) {
                CameraCharacteristics cameraChars = manager.getCameraCharacteristics(cameraId);

                if (cameraChars.get(CameraCharacteristics.LENS_FACING) == CameraMetadata.LENS_FACING_BACK)
                    return cameraId;
            }
        }
        catch (CameraAccessException e) {
            System.out.println("Couldn't access camera!");
            e.printStackTrace();
        }

        return "";
    }

    private TextureView.SurfaceTextureListener surfaceTextureListener = new TextureView.SurfaceTextureListener() {
        @Override
        public void onSurfaceTextureAvailable(SurfaceTexture surfaceTexture, int width, int height) {
            mPreview = new Surface(surfaceTexture);

            CameraManager manager = (CameraManager) getSystemService(Context.CAMERA_SERVICE);
            String camId = getBackCameraId(manager);

            try {
                mSizes = manager.getCameraCharacteristics(camId).get(CameraCharacteristics.SCALER_STREAM_CONFIGURATION_MAP).getOutputSizes(SurfaceTexture.class);
                manager.openCamera(getBackCameraId(manager), camStateCallback, null);
            }
            catch(CameraAccessException e) {
                System.out.println("Couldn't access camera!");
                e.printStackTrace();
            }
        }

        @Override
        public boolean onSurfaceTextureDestroyed(SurfaceTexture surface) {
            return false;
        }

        @Override
        public void onSurfaceTextureSizeChanged(SurfaceTexture surface, int width, int height) {}

        @Override
        public void onSurfaceTextureUpdated(SurfaceTexture surface) {}
    };

    private CameraDevice.StateCallback camStateCallback = new CameraDevice.StateCallback() {
        @Override
        public void onOpened(CameraDevice camera) {
            mCamera = camera;

            try {
                SurfaceTexture surfaceTexture = mTextureView.getSurfaceTexture();
                surfaceTexture.setDefaultBufferSize(mSizes[2].getWidth(), mSizes[2].getHeight());

                List<Surface> surfaces = Arrays.asList(mPreview);
                mRequestBuilder = camera.createCaptureRequest(CameraDevice.TEMPLATE_PREVIEW);
                mRequestBuilder.addTarget(mPreview);

                camera.createCaptureSession(surfaces, camCaptureSessionStateCallback, new Handler());

            }
            catch (CameraAccessException e) {
                e.printStackTrace();
            }
        }

        @Override
        public void onDisconnected(CameraDevice camera) {}

        @Override
        public void onError(CameraDevice camera, int error) {}
    };

    private CameraCaptureSession.StateCallback camCaptureSessionStateCallback = new CameraCaptureSession.StateCallback() {
        @Override
        public void onConfigured(CameraCaptureSession session) {
            try {
                session.setRepeatingRequest(mRequestBuilder.build(), camCaptureSessionCallback, new Handler());
            }
            catch (CameraAccessException e) {
                e.printStackTrace();
            }
        }

        @Override
        public void onConfigureFailed(CameraCaptureSession session) {}
    };

    private CameraCaptureSession.CaptureCallback camCaptureSessionCallback = new CameraCaptureSession.CaptureCallback() {};

    public void onPause() {
        super.onPause();

        if (mCamera != null) {
            mCamera.close();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
/*
    public void sendBearings(View view) {
        Intent intent = new Intent(this, DistanceCalculator.class);
        Button button = (Button) findViewById(R.id.test_button);
        String message = "This is a test message";
        intent.putExtra("aMessage", message);
        startActivity(intent);
    }
*/
}
